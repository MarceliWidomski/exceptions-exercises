//============================================================================
// Name        : exception6.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "myArray.h"

using namespace std;

int main() {
	try {
		cout << "Enter array size: ";
		int size;
		cin >> size;
		myArray myArray(size);
		cout << "Print element from index: ";
		int index;
		cin >> index;
		cout << "myArray[" << index << "] = " << myArray.at(index) << endl;
		cout << "Enter new size of myArray: ";
		int newSize;
		cin >> newSize;
		myArray.resize(newSize);
		cout << "Enter value to append: ";
		int toAppend;
		cin >> toAppend;
		myArray.append(toAppend);
		cout << "myArray = [";\
		myArray.print();
		cout << "]" << endl;
	} catch (myArray::InvalidIndex& e) {
		cout << e.what() << endl;
	} catch (myArray::InvalidSize& e) {
		cout << e.what() << endl;
	}
	return 0;
}

//1. class myArray, storages integers
//2. class functions:
//at(int x) return element at x index
//resize (int new_size) resizes myArray
//append(int x) appends x value to array (array size increases by 1
// 3. myArray has its own exception types
