/*
 * myArray.cpp
 *
 *  Created on: 17.05.2017
 *      Author: RENT
 */

#include "myArray.h"
#include <iostream>
#include <typeinfo>

myArray::myArray() {
	size = 0;
	array = 0;
}
myArray::myArray(int size) {
	try {
		this->size = size;
		array = new int[size];
		for(int i =0; i<size; ++i){
			array[i] = i;
		}
	} catch (std::bad_alloc& bad_alloc) {
		InvalidSize e;
		throw e;
	}
}
myArray::~myArray() {
	delete[] array;
}
int myArray::at(int index) {
	if (index < 0 || index >= size) {
		InvalidIndex e;
		throw e;
	} else
		return array[index];
}
void myArray::resize(int newSize) {
	try {
		int *newArray = new int[newSize];
		if (array) {
			int counter = size;
			if (newSize < size)
				counter = newSize;
			for (int i = 0; i < newSize && i < newSize; ++i)
				newArray[i] = array[i];
			delete[] array;
		}
		array = newArray;
		size = newSize;
	} catch (std::bad_alloc& bad_alloc) {
		InvalidSize e;
		throw e;
	}
}
void myArray::append(int value) {
	resize(size + 1);
	array[size - 1] = value;
}
void myArray::print(){
	for(int i = 0; i<size; ++i)
		std::cout << array[i] << " ";
}
