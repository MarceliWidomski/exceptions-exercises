/*
 * myArray.h
 *
 *  Created on: 17.05.2017
 *      Author: RENT
 */

#ifndef MYARRAY_H_
#define MYARRAY_H_

#include <exception>

class myArray {
public:
	class InvalidIndex: public std::exception {
	public:
		const char* what() {
			return "InvalidIndex";
		}
	};
	class InvalidSize: public std::exception {
	public:
		const char* what(){
			return "InvalidSize";
		}
	};
	myArray();
	myArray(int size);
	virtual ~myArray();

	int at(int index);
	void resize(int newSize);
	void append(int value);
	void print();
//private:
	int size;
	int *array;

};

#endif /* MYARRAY_H_ */
