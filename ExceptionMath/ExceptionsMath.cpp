//============================================================================
// Name        : ExceptionsMath.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "MathWithExceptions.h"
#include <cmath>

int main() {
	try {
//		std::cout << math::MathWithExceptions::pow(-2, 0.5); // throws domain exception
//		std::cout << math::MathWithExceptions::log(0); // throws range exception
		std::cout << math::MathWithExceptions::tan(-5*M_PI/2.0); // throws domain exception
	} catch (math::DomainError& e) {
		std::cout << e.what() << std::endl;
	} catch (math::RangeError& e) {
		std::cout << e.what() << std::endl;
	}
	return 0;
}

//1. Implement class MathWithExceptions which is wrapper for cmath library
//2. Class has methods:
//	-log
//	-pow
//	-sin
//	-cos
//	-tan
//3. Class has own exceptions types
//	-domain error
//	-range error
//	-exception should inform what is possible solution (e.g. too large arguments)
//4. use of own namespace math
//5. Class methods can be used without creating any object of the class
//6. In main function possible exception should be caught
