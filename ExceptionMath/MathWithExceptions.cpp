/*
 * MathWithExceptions.cpp
 *
 *  Created on: 18.05.2017
 *      Author: RENT
 */

#include "MathWithExceptions.h"
#include <iostream>
#include <cmath>
#include <limits>

namespace math {

double MathWithExceptions::pow(double x, double y) {
	double result = ::pow(x, y);
	if (errno == EDOM) {
		DomainError e("Argument(s) not within domain");
		throw e;
	}
	if (errno == ERANGE) {
		RangeError e("Too large argument(s)");
		throw e;
	}
	return result;
}
double MathWithExceptions::log(double x) {
	double result = ::log(x);
	if (errno == EDOM) {
		DomainError e("Log(x) can be computed only if x is positive");
		throw e;
	}
	if (errno == ERANGE) {
		RangeError e("Argument value is to close to 0");
		throw e;
	}
	return result;
}
double MathWithExceptions::sin(double x) {
	double result = ::sin(x);
	return result;
}
double MathWithExceptions::cos(double x) {
	double result = ::cos(x);
	return result;
}
double MathWithExceptions::tan(double x) {
	double result = ::tan(x);
	int integerK = x / (M_PI / 2);
	double doubleK = x / (M_PI / 2);
	if (std::abs(doubleK - integerK) < std::abs(doubleK * std::numeric_limits<double>::epsilon())) {
		DomainError e("for k = integer, k*(M_PI/2) not within domain");
		throw e;
	}
	return result;
}

} /* namespace math */
