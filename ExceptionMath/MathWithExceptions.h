/*
 * MathWithExceptions.h
 *
 *  Created on: 18.05.2017
 *      Author: RENT
 */

#ifndef MATHWITHEXCEPTIONS_H_
#define MATHWITHEXCEPTIONS_H_

#include <string>
#include <exception>

namespace math {
class DomainError : public std::exception {
public:
	DomainError(std::string message): text("DoimanError: "+message){}
	virtual ~DomainError() throw(){}
	const char* what(){return text.c_str();}
	const std::string& getMessage() const {return text;}
private:
	std::string text;
};
class RangeError : public std::exception {
public:
	RangeError(std::string message) : text("RangeError: "+message){}
	virtual ~RangeError()throw(){}
	const char* what(){return text.c_str();}

	const std::string& getMessage() const {return text;}
	std::string text;
};
class MathWithExceptions {
public:
	MathWithExceptions();
	virtual ~MathWithExceptions();
	static double pow(double x, double y);
	static double log(double x);
	static double sin (double x);
	static double cos (double x);
	static double tan (double x);
};
} /* namespace math */
#endif /* MATHWITHEXCEPTIONS_H_ */
