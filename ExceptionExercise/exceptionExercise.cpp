//============================================================================
// Name        : exception5.cpp
// Author      :
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <cmath>
#include <exception>

using namespace std;

class ErangeException: public exception {
public:
	const char* what() {
		return "ERANGE exception";
	}
};
class EdomException: public exception {
public:
	const char* what() {
		return "EDOM exception";
	}
};

double myPow(double x, double y) {
	double result = pow(x, y);
	if (errno == EDOM) {
		EdomException e;
		throw e;
	}
	if (errno == ERANGE) {
		ErangeException e;
		throw e;
	}
	return result;
}

int main() { // math wrapper
	try {
		double a = myPow(-2, 0.5);
		cout << "a = " << a << endl;
	} catch (EdomException& e) {
		cout << e.what() << endl;

	} catch (ErangeException& e) {
		cout << e.what() << endl;
	}
	return 0;
}
